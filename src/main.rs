extern crate sdl2;
extern crate ndarray;
extern crate num;

use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Point;
use std::time::Duration;
use ndarray::{Array, Array2};
use num::Complex;

const SIZE_X: u32 = 800;
const SIZE_Y: u32 = 600;

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("rust-sdl2 demo: Video", SIZE_X, SIZE_Y)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    let real = Array::linspace(-2., -1.5, SIZE_X as usize);
    let imag = Array::linspace(-0.1, 0.1, SIZE_Y as usize);
    let mut c = Array2::zeros((SIZE_X as usize, SIZE_Y as usize));
    let mut data = Array2::<Complex<f64>>::zeros((SIZE_X as usize, SIZE_Y as usize));
    let mut color = Array2::<u8>::zeros((SIZE_X as usize, SIZE_Y as usize));

    for ((x, y), e) in c.indexed_iter_mut() {
        *e = Complex::new(*real.get(x).unwrap(), *imag.get(y).unwrap());
    }

    let c = c;

    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut count = 0;

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => break 'running,
                _ => {}
            }
        }
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));

        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        for ((x, y), e) in data.indexed_iter_mut() {
            if e.norm() < 2. {
                *e = (*e) * (*e) + c.get((x, y)).unwrap();
            }
        }
        for ((x, y), v) in color.indexed_iter_mut() {
            if data.get((x, y)).unwrap().norm() < 2. {
                *v = count;
            }
            canvas.set_draw_color(Color::RGB(*v, *v, *v));
            canvas.draw_point(Point::new(x as i32, y as i32)).unwrap();
        }
        canvas.present();
        if count < 235 {
            count = count + 20;
        }
    }
}
